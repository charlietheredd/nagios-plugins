#!/bin/bash
#
# This script takes input and does a date comparison from what is 
# inputted compared from todays system date. It outputs how many days until 
# the domain is set to expire. It does not do any sanity checking, you've been 
# warned. 
#
# This script is useful for when you have a domain that doesn't have a registry 
# with a expiration on the whois, as there is already scripts out there that can 
# more dynamically check that and you should use that instead of this unless its not
# working. 
# 
# Syntax: ./check_domain_basic.sh <domain> <expire date> <warn expire days> <crit expire days>
# Example: ./check_domain_basic.sh domain.com 01-Feb-2021 30 15
#

# Seconds In A Day
DAY=86400
# Domain Input
DOMAIN=$1
# Date Input
E=$2
EXPIREDATE=`date --date=$2 +%s`
# Warning Threshold In Days (converted to epoch)
W=$3
WARN=$((W * DAY))
# Critical Threshold In Days (converted to epoch)
C=$4
CRIT=$((C * DAY))
# Today's Date (converted to epoch)
D=`date +%d-%b-%y`
TODAYDATE=`date -d $D +%s`

# Test For Expiration
CRITICALDATE=$((EXPIREDATE - CRIT))
WARNINGDATE=$((EXPIREDATE - WARN))
DAYSLEFT=$(((EXPIREDATE - TODAYDATE) / DAY))
if [[ $TODAYDATE -ge $CRITICALDATE ]]
  then
    echo -e "Critital: $DOMAIN expires in $DAYSLEFT days on `echo $EXPIREDATE | awk '{print strftime("%d-%b-%Y",$1)}'`"
    exit 2
  elif [[ $TODAYDATE -ge $WARNINGDATE ]];
    then
    echo -e "Warning: $DOMAIN expires in $DAYSLEFT days on `echo $EXPIREDATE | awk '{print strftime("%d-%b-%Y",$1)}'`"
    exit 1
  else
    echo -e "OK: $DOMAIN expires in $DAYSLEFT days on `echo $EXPIREDATE | awk '{print strftime("%d-%b-%Y",$1)}'`"
    exit 0
fi
